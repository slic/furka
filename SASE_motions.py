from time import sleep
import numpy as np
from epics import PV

from logzero import logger as log

from slic.core.adjustable import Adjustable
from slic.core.adjustable import PVAdjustable
from slic.core.scanner.scanbackend import wait_for_all #, stop_all
from slic.devices.general.motor import Motor



class SASE_coupled_camera(Adjustable):
 

    def __init__(self, ID="SASE YAG Coupled Camera", units="mm", delta=0, name="" ):
          super().__init__(ID, name=name, units=units)
          self.cam_y = Motor("SATOP11-PSAS079:MOT_DDCAM_Y")
          self.cryst_y = Motor("SATOP11-PSAS079:MOT_DDC_Y")
          self.delta = delta
 
 
    def set_target_value(self, value):
          s_cryst_y  = value
          s_cam_y = value + self.delta ## delta = -41.149 
 
          t_cryst_y = self.cryst_y.set_target_value(s_cryst_y)
          t_cam_y = self.cam_y.set_target_value(s_cam_y)
          t_cryst_y.wait()
          t_cam_y.wait()
    def get_current_value(self):
          return self.cryst_y.get_current_value()
    def is_moving(self):
          return any([self.cryst_y.is_moving(),self.cam_y.is_moving()])
