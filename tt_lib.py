def tt_off_acq(shots):
	import numpy as np
	from epics import caget, caput, cainfo 
	import time
	data = caget('SATES31-CAMS187-RIXS1:FPICTURE')
	x_size = int(caget('SATES31-CAMS187-RIXS1:CAMROI_X_END')-caget('SATES31-CAMS187-RIXS1:CAMROI_X_START')+1)
	y_size = int(len(data)/x_size)
	ave = []

	for i in range(shots):
		data = caget('SATES31-CAMS187-RIXS1:FPICTURE') 
		ROI = [data[j:j+x_size] for j in range(0,int(len(data)),x_size)] 
		ROI = np.transpose(ROI)
		x_prof = []
		for j in range(len(ROI)):
			x_prof.append(np.sum(ROI[j]))
		ave.append(x_prof)
	ave = np.nanmean(ave,axis=0)
	return (ave)

def tt_live(off,sleep=0.1):
	import numpy as np
	from epics import caget, caput, cainfo 
	from matplotlib import pyplot as plt
	import time

	data = caget('SATES31-CAMS187-RIXS1:FPICTURE')
	x_size = int(caget('SATES31-CAMS187-RIXS1:CAMROI_X_END')-caget('SATES31-CAMS187-RIXS1:CAMROI_X_START')+1)
	y_size = int(len(data)/x_size) 

	xx = np.arange(1,x_size+1,1)
	ROI = [data[i:i+x_size] for i in range(0,int(len(data)),x_size)] 
	ROI = np.transpose(ROI) 
	x_prof = []
	for i in range(len(ROI)):
		x_prof.append(np.sum(ROI[i]))
	pp = (np.array(x_prof)/np.array(off)-1)*100

	plt.ion()
	fig = plt.figure()
	ax = fig.subplots(nrows=1,ncols=1)
	tt, = ax.plot(xx,pp)

	plt.title('timing-tool live x profile in ROI',fontsize=20)
	plt.xlabel('pixel',fontsize=16)
	plt.ylabel('(on-off)/off [%]',fontsize=16)
	plt.grid()

	for _ in iter(int,1):
		data = caget('SATES31-CAMS187-RIXS1:FPICTURE') 
		ROI = [data[i:i+x_size] for i in range(0,int(len(data)),x_size)] 
		ROI = np.transpose(ROI) 
		x_prof = []
		for i in range(len(ROI)):
			x_prof.append(np.sum(ROI[i]))
		pp = (np.array(x_prof)/np.array(off)-1)*100
		tt.set_ydata(np.array(pp))
		fig.canvas.draw()
		fig.canvas.flush_events()
#		plt.ylim(np.min(pp),np.max(pp))
		plt.ylim(-30,5)
		time.sleep(sleep)

def PCO_live_x(sleep=0.1):
	import numpy as np
	from epics import caget, caput, cainfo 
	from matplotlib import pyplot as plt
	import time

	data = caget('SATES31-CAMS187-RIXS1:FPICTURE')
	x_size = int(caget('SATES31-CAMS187-RIXS1:CAMROI_X_END')-caget('SATES31-CAMS187-RIXS1:CAMROI_X_START')+1)
	y_size = int(len(data)/x_size) 

	xx = np.arange(1,x_size+1,1)
	ROI = [data[i:i+x_size] for i in range(0,int(len(data)),x_size)] 
	ROI = np.transpose(ROI) 
	x_prof = []
	for i in range(len(ROI)):
		x_prof.append(np.sum(ROI[i]))

	plt.ion()
	fig = plt.figure()
	ax = fig.subplots(nrows=1,ncols=1)
	tt, = ax.plot(xx,x_prof)

	plt.title('live x profile in ROI',fontsize=20)
	plt.xlabel('pixel',fontsize=16)
	plt.ylabel('intensity [arb. units]',fontsize=16)

	for _ in iter(int,1):
		data = caget('SATES31-CAMS187-RIXS1:FPICTURE') 
		ROI = [data[i:i+x_size] for i in range(0,int(len(data)),x_size)] 
		ROI = np.transpose(ROI) 
		x_prof = []
		for i in range(len(ROI)):
			x_prof.append(np.sum(ROI[i]))
		tt.set_ydata(np.array(x_prof))
		fig.canvas.draw()
		fig.canvas.flush_events()
		plt.ylim(np.min(x_prof),np.max(x_prof))
		time.sleep(sleep)

def PCO_live(sleep=0.1):
	import numpy as np
	from epics import caget, caput, cainfo 
	from matplotlib import pyplot as plt
	import time

	data = caget('SATES31-CAMS187-RIXS1:FPICTURE')
	x_size = int(caget('SATES31-CAMS187-RIXS1:CAMROI_X_END')-caget('SATES31-CAMS187-RIXS1:CAMROI_X_START')+1)
	y_size = int(len(data)/x_size) 

	xx = np.arange(1,x_size+1,1)
	ROI = [data[i:i+x_size] for i in range(0,int(len(data)),x_size)] 

	plt.ion()
	fig = plt.figure()
	image = plt.imshow(ROI)
	plt.colorbar()
	plt.clim(0,np.max(data))

	plt.title('live PCO image',fontsize=20)
	plt.xlabel('x [px]',fontsize=16)
	plt.ylabel('y [px]',fontsize=16)

	for _ in iter(int,1):
		data = caget('SATES31-CAMS187-RIXS1:FPICTURE') 
		ROI = [data[i:i+x_size] for i in range(0,int(len(data)),x_size)] 
		image.set_data(ROI)
		fig.canvas.draw()
		fig.canvas.flush_events()
		plt.clim(0,np.max(data))
		time.sleep(sleep)

