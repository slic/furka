#!/usr/bin/env python

from slic.core.acquisition import SFAcquisition
from slic.core.acquisition import PVAcquisition
from slic.core.acquisition import BSChannels, PVChannels, DetectorConfig
#from slic.core.acquisition import FakeAcquisition
from slic.core.adjustable import PVAdjustable, DummyAdjustable
from slic.core.condition import PVCondition
from slic.core.device import SimpleDevice
from slic.core.scanner import Scanner
from slic.devices.general.delay_stage import DelayStage
from slic.devices.general.motor import Motor
from slic.devices.general.smaract import SmarActAxis
from slic.gui import GUI
from slic.utils import devices
from slic.utils import as_shortcut, Marker

from spreadsheet import overview, print_overview, print_line_for_spreadsheet
from channels import channels, pvs
from sensors  import *

#from undulator import Undulators
#from undulator import Mono
#from undulator import Coupled_MonoUnd

mono_new = PVAdjustable("SATOP11-OSGM087:SetEnergy", "SATOP11-OSGM087:photonenergy", accuracy=0.1, process_time=10, name="Mono new")


from tth import Coupled_tth, Coupled_tth_outer, Coupled_tth_RIXSside
from tth import LakeShore
from tth import Coupled_THzGate
from tth import Coupled_THzGate_fixedDelta

from SASE_motions import SASE_coupled_camera

from qspace import QSpace3D
from qspace import Wavelength
from qspace import HistoryDummy

from constraints import ExtraConstraint

from sasespec import sasespecs



from epics import caput

class ParkableMotor(Motor):
     def park(self):
         kill_pvname = self.pvname +  "_KILL"
         caput(kill_pvname, 1)        



dummy = DummyAdjustable(units="au")


from slic.devices.general.smaract import SmarActAxis
parab_RY = Motor("SATES30-MCS001:MOT_4", name="Parab RY")
parab_RX = Motor("SATES30-MCS001:MOT_7", name="Parab RX")
taget_along_beam = SmarActAxis("SATES30-XSMA184:MOT6", name="Target along beam")

#Retro Chamber Motors
#mot_x = Motor("SATES30-RETRO:MOT_X", name="Retro X")
#mot_y = Motor("SATES30-RETRO:MOT_Y", name="Retro Y")
#mot_z = Motor("SATES30-RETRO:MOT_Z", name="Retro Z")
#mot_theta = Motor("SATES30-RETRO:MOT_RY", name="Retro Theta")
#retro = SimpleDevice("Retro Stages", x=mot_x, y=mot_y, z=mot_z, theta=mot_theta)


#Furka Kbs motors
KBv_RX = PVAdjustable("SATOP31-OKBV178:W_RX.VAL", pvname_moving="SATOP31-OKBV178:MOVING", name = "KB Ver RX")
KBv_B1 = PVAdjustable("SATOP31-OKBV178:BU.VAL",   pvname_moving="SATOP31-OKBV178:MOVING", name = "KB Ver B1")
KBv_B2 = PVAdjustable("SATOP31-OKBV178:BD.VAL",   pvname_moving="SATOP31-OKBV178:MOVING", name = "KB Ver B2")


KBh_RY = PVAdjustable("SATOP31-OKBH179:W_RY.VAL", pvname_moving="SATOP31-OKBH179:MOVING", name = "KB Horiz RY")
KBh_B1 = PVAdjustable("SATOP31-OKBH179:BU.VAL",   pvname_moving="SATOP31-OKBH179:MOVING", name = "KB Hor B1")
KBh_B2 = PVAdjustable("SATOP31-OKBH179:BD.VAL",   pvname_moving="SATOP31-OKBH179:MOVING", name = "KB Hor B2")



## PMOS adjustables
PMOS127_FOCUS = PVAdjustable("SLAAT-GSLENS0:SET_CTS_POWER", process_time=1, name="PMOS127_FOCUS")
PMOS132_FOCUS = PVAdjustable("SLAAT-GSLENS1:SET_CTS_POWER", process_time=1, name="PMOS132_FOCUS")
#PMOS_FOCUS = PVAdjustable("SLAAT-GSLENS1:SET_CTS_POWER", process_time=1, name="PMOS_FOCUS")
MONO_G_rad = PVAdjustable("SATOP11-OSGM087:G_RADIUS", pvname_moving="SATOP11-OSGM087:MONO_STATUS", name="MONO_G_rad")
MONO_M_rad = PVAdjustable("SATOP11-OSGM087:M_RADIUS", pvname_moving="SATOP11-OSGM087:MONO_STATUS", name="MONO_M_rad")
MONO_M_curv = PVAdjustable("SATOP11-OSGM087:M_CURVATURE", pvname_moving="SATOP11-OSGM087:MONO_STATUS", name="MONO_M_curv")

## OAPU085
OAPU085_vert = PVAdjustable("SATOP11-OAPU085:MOTOR_Y.VAL", process_time=1, name="OAPU085_vert")

##mono_slits = PVAdjustable("SATOP11-OSGM087:EXITSLIT",pvname_done_moving="SATOP31-OEXS132:MOT_H.DMOV", name = "Mono_Slits" )

##undulators set-up
#n_und_ref = 13
#n_unds = [
#    6, 7, 8, 9, 10, 11, 12, 13, # 14 is the CHIC
#    15, 16, 17, 18, 19, 20, 21, 22
#]
#chic_fudge_offset = 0
#Mon2Unds_offset = 3.0

#und = Undulators(n_unds, n_und_ref, chic_fudge_offset, name="z Athos Undulators")
##und = Undulators(name="Undulators")


##Mono set up
#mono_name =  "Athos_mono"
#pv_mono_name="SATOP11-OSGM087" 

##Mon = PVAdjustable("SATOP11-OSGM087:SetEnergy", pvname_done_moving="SATOP11-OSGM087:MOVING", name="MONO")
#Mon = Mono(pv_mono_name=pv_mono_name, mono_name=mono_name)
#MonUnd = Coupled_MonoUnd(
#    n_unds, n_und_ref, chic_fudge_offset, unds_name="z Athos Undulators",
#    pv_mono_name=pv_mono_name, mono_name=mono_name, delta=Mon2Unds_offset,
#    name="Mono+Und"
#)

Mon_ES = PVAdjustable("SATOP11-OSGM087:EXITSLIT", pvname_done_moving="SATOP31-OEXS132:MOT_H.DMOV", name = "Mono ES")
gas_attenuator_trans = PVAdjustable("SATFE10-OGAT053:TRANSMISSION", process_time=20, name="Gas Attenuator Transmission")


lakeshore = PVAdjustable("SATES30-LS336:LOOP1_SP", "SATES30-LS336:A_RBV", accuracy=0.25, name="Lakeshore Temp")


#att = Attenuator("SATFE10-OATT064")
#shutter = Shutter("SATOP31-OPSH138")




#lakeshore = LakeShore(name="Temperature")


#wl = Wavelength(Mon)
wl = Wavelength(mono_new)



##Diffractometer motions
mu = ParkableMotor("SATES30-ARES:MOT_SRY",  name = "Diff RY")
chi = ParkableMotor("SATES30-ARES:MOT_SRZ", name = "Diff RZ")
phi = ParkableMotor("SATES30-ARES:MOT_SRX", name = "Diff RX")
nu = ParkableMotor("SATES30-ARES:MOT_DRY",  name = "Diff DRY")
#q = QSpace3D("SOMETHING:Q", mu, chi, phi, nu, wl)

TX = ParkableMotor("SATES30-ARES:MOT_STX", name = "Diff TX")
TY = ParkableMotor("SATES30-ARES:MOT_STY", name = "Diff TY")
TZ = ParkableMotor("SATES30-ARES:MOT_STZ", name = "Diff TZ")
TwoTRY=ParkableMotor("SATES30-ARES:MOT_2TRY", name = "Diff 2TRY")


tth_scan = Coupled_tth(delta=0,  name="theta 2theta")
tth_scan_RIXS = Coupled_tth_RIXSside(delta=0, name='theta 2theta RIXS side')
tth_scan_outer = Coupled_tth_outer(delta=0,  name="theta 2theta outer")
Thz_Gate_scan = Coupled_THzGate( name = "THz Gate coupled ")
Thz_Gate_scan_fixedD =  Coupled_THzGate_fixedDelta(delta=73.11, name = "Thz Gate fix Delta")

GRX = Motor("SATES30-RIXS:MOT_GRX", name = "Pitch grating")
DTZ = Motor("SATES30-RIXS:MOT_DTZ", name = "DTZ (r2)")


SASE_camera_scan = SASE_coupled_camera(delta=-41.149,  name="SASE YAG Coupled Camera")

#fake_mu  = HistoryDummy.init_from(mu)
#fake_chi = HistoryDummy.init_from(chi)
#fake_phi = HistoryDummy.init_from(phi)
#fake_nu  = HistoryDummy.init_from(nu)
#fake_q = QSpace3D("FAKE:Q", fake_mu, fake_chi, fake_phi, fake_nu, wl)
##fake_q.set_lattice("FAKE:Q", fake_mu, fake_chi, fake_phi, fake_nu, wl)



#Laser motors

lxt = PVAdjustable("SLAAT01-LTIM-PDLY:DELAY", pvname_done_moving="SLAAT01-LTIM-PDLY:WAITING", name="LXT")

PICO_X1 = PVAdjustable("SLAAT31-LMNP-PICO11:DRIVE",  name="PICO X1")
PICO_Y1 = PVAdjustable("SLAAT31-LMNP-PICO12:DRIVE",  name="PICO Y1")

Thz_delay  = Motor("SLAAT31-LMOT-M806:MOT", name="Thz Delay")
WL_delay   = Motor("SLAAT31-LMOT-M807:MOT", name="WL Delay")
gate_delay = Motor("SLAAT31-LMOT-M808:MOT", name="Gate Delay")
twomicron_delay = Motor("SLAAT31-LMOT-M813:MOT", name="2um Delay")
ss_EO_gate = PVAdjustable("SLAAT31-LMTS-SMAR13:DRIVE", pvname_readback="SLAAT31-LMTS-SMAR13:MOTRBV", name="ss EO Gate Delay")

#laser_WP = Motor("SLAAT31-LMOT-M801:MOT", name="Laser WavePlate")
laser_WP = Motor("SLAAT31-LMOT-M809:MOT", name="Laser WavePlate")
laser_fluence = Motor("SLAAT31-LMOT-M801:MOT", name="Laser fluence")





#overview.Thz_delay = Thz_delay


from spreadsheet import overview, print_overview, print_line_for_spreadsheet, spreadsheet_info
from slic.core.acquisition.spreadsheet import Spreadsheet

spreadsheet = Spreadsheet(spreadsheet_info, placeholders=["comment", "sample"], host="satesf-cons-03", port=9090)




instrument = "furka"
pgroup = "p21972"  #User p group

#check_intensity = PVCondition("SATFE10-PEPG046:FCUP-INTENSITY-CAL", vmin=5, vmax=None, wait_time=3, required_fraction=0.8)
check_intensity = PVCondition("SATBD01-DBPM060:Q2", vmin=5, vmax=None, wait_time=1, required_fraction=0.8)
#check_intensity = None



detectors = DetectorConfig("JF18T01V01")
#detectors = None #Remove this line to enable Jungfrau!

daq = SFAcquisition(instrument, pgroup, default_channels=channels, default_pvs=pvs, default_detectors=detectors, rate_multiplicator=1, spreadsheet=spreadsheet)
daq.update_config_pvs()  
#daq = FakeAcquisition(instrument, pgroup, default_channels=channels, default_pvs=pvs, rate_multiplicator=1)
#daq = BSAcquisition .... TBI

#daqPV = PVAcquisition(instrument, pgroup, default_channels=live_channels)
scan = Scanner(default_acquisitions=[daq], condition=check_intensity, default_sensor=s3)

gui = GUI(scan, show_goto=True, show_spec=True, show_run=True)
guitest = GUI(scan, show_goto=True, show_spec=True, show_run=True, title="Test GUI")


#scanPV = Scanner(default_acquisitions=[daqPV], condition=check_intensity)


# Test park function motor




'''
Button that runs a function
'''
@as_shortcut
def test(name="TEST"):
    print("test")
# use marker() to go to a marker position

@as_shortcut
def dump_status_to_stand():
    daq.spreadsheet.add(None, None, None)

'''
Single marker
'''
m1 = Marker(dummy,value=25,name='Normal IN')




def Diffr_pos(TRYv, DRYv, TXv , TYv, TZv, RXv, RYv, RZv):
    print(f"2TRY = {TRYv}, DRY = {DRYv}, TX = {TXv} , TY = {TYv}, TZ = {TZv}, RX = {RXv}, RY = {RYv}, RZ = {RZv}")
    t1 = TwoTRY.set_target_value(TRYv)
    t2 = nu.set_target_value(DRYv)
    t3 = TX.set_target_value(TXv)
    t4 = TY.set_target_value(TYv)
    t5 = TZ.set_target_value(TZv)
    t6 = phi.set_target_value(RXv)
    t7 = mu.set_target_value(RYv)
    t8 = chi.set_target_value(RZv)

    ts = [t1, t2, t3, t4, t5, t6, t7, t8]
    for t in ts:
        t.wait()
    print("moving done")
    pms=[TwoTRY, nu, TX, TY, TZ, phi, mu, chi]
    for pm in pms:
        pm.park()
    print("done")

def KBv_Focusing(KBv_B1v, KBv_B2v):
    KBv_B1.set_target_value(KBv_B1v-0.15).wait() 
    KBv_B1.set_target_value(KBv_B1v).wait()

    KBv_B2.set_target_value(KBv_B2v-0.15).wait()
    KBv_B2.set_target_value(KBv_B2v).wait()
    
    print("KBv moving done")

def KBh_Focusing(KBh_B1v, KBh_B2v):
    KBh_B1.set_target_value(KBh_B1v-0.15).wait() 
    KBh_B1.set_target_value(KBh_B1v).wait()

    KBh_B2.set_target_value(KBh_B2v-0.15).wait()
    KBh_B2.set_target_value(KBh_B2v).wait()
    
    print("KBh moving done")

def KBv_Pointing(KBv_RXv):
    KBv_RX.set_target_value(KBv_RXv-0.15).wait() 
    KBv_RX.set_target_value(KBv_RXv).wait()
    print("KBv pointing change done")

def KBh_Pointing(KBh_RYv):
    KBh_RY.set_target_value(KBh_RYv-0.15).wait() 
    KBh_RY.set_target_value(KBh_RYv).wait()
    print("KBh pointing change done")


def GoToXAS_RIXS(RYv, TwoTRYv, Del_or_Norm, ESv, GAv, MonoNewv, HWPv, KBv_B1v, KBv_B2v, KBh_B1v, KBh_B2v, KBv_RXv, KBh_RYv, DC_updownv ):
    #close shutter
    caput('SATOP31-OPSH138:REQUEST', 'close')
    #caput('SATES30-MCS003:MOT_5.VAL', DC_updownv)
    #print("DC target moved")
    KBv_Focusing(KBv_B1v-0.1, KBv_B2v-0.1)
    KBv_Focusing(KBv_B1v, KBv_B2v)
    KBv_Pointing(KBv_RXv)
    KBh_Focusing(KBh_B1v-0.1, KBh_B2v-0.1)
    KBh_Focusing(KBh_B1v, KBh_B2v)
    KBh_Pointing(KBh_RYv)
    print("KB pointing done")
    TwoTRY.set_target_value(TwoTRYv).wait()
    print("2TRY done")
    mu.set_target_value(RYv)
    print("RY movement done")
    if Del_or_Norm == "Del":
        caput('SLAAT01-LTIM-PDLY:DELAYEDSHOT.PROC', 1 )
    else:
        caput('SLAAT01-LTIM-PDLY:NORMAL.PROC', 1 )
    print("delay mode done")
    Mon_ES.set_target_value(ESv).wait() ## comment this out if ES error
    print("Exit slit done")
    gas_attenuator_trans.set_target_value(GAv).wait()
    print("GA done")
    mono_new.set_target_value( MonoNewv).wait()
    print("Mono done")
    laser_WP.set_target_value(HWPv).wait()
    print("HWP done")
    print("all done")
    return 1

   
@as_shortcut
def TrXAS_pos(name = "TrXAS Positions"):
     GoToXAS_RIXS(-44.8, 45, "Del", 400, 1.0, 578.5, 22.0, 3.8459, 3.8378, 3.8200, 4.0200, 13.1998, 13.11231, 0.0) 

@as_shortcut
def RIXS_pos(name = "RIXS Positions"):
     GoToXAS_RIXS(0.0,50, "Norm", 70, 0.2, 576.8, 22.0, 3.1890, 3.340, 3.810, 4.010, 13.188, 13.128, 4)

#@as_shortcut
#def EOS_pos(name = "EOS Positions"):
#    Diffr_pos(148.26, 80.5, 1.3, 5.12, -3.0, 0, -90, -4.0)

#@as_shortcut
#def CTape_pos(name = "X-ray knife edge"):
#    Diffr_pos(85, 95, -1, -1.6, -1.5, 0, -30, 0)

#@as_shortcut
#def xray_knife(name = "X-ray knife edge"):
#    Diffr_pos(125, 165.3, 0, 2.0, -6.5, 0, -90, -3.0)

#@as_shortcut
#def Sample_position_RIXS(name = "Sample Positions RIXS"):
#    Diffr_pos(85.0, 80.5, -3.6, -1.3, -3, 0, -8, -4)

#@as_shortcut
#def EOS_LV_pos(name = "EOS LV Positions"):
#     Diffr_pos(149.440, 29.5, 3.0, 2.5, -0.69, 15, 82, 0)

#@as_shortcut
#def Transfer_pos(name = "Transfer Positions"):
#      Diffr_pos(148.40, 80.5, 4.21, -2.0, -0.5, 0, 60, 0)

print("To start the GUI, run: gui()")



