import time

#Starting temperature setpoint 202K

laser_delay.set_target_value(112).wait()

mu.set_target_value(21.3).wait()
nu.set_target_value(-125.8).wait()

#Theta scan
scan.scan1D(mu, 20,24,0.2, 1200, "Th_cm", return_to_initial_values=True) 

#DS @ CM
scan.scan1D(laser_delay, 107, 110.2, 0.2,  2400, "DS_range1", return_to_initial_values=False) 
scan.scan1D(laser_delay, 110.21, 110.5, 0.01,  2400, "DS_range2", return_to_initial_values=False) 
scan.scan1D(laser_delay, 111, 130, 0.5,  2400, "DS_range3", return_to_initial_values=False) 

#DS @ ICM
mu.set_target_value(22.5).wait()
nu.set_target_value(-125.4).wait()
scan.scan1D(laser_delay, 107, 110.2, 0.2,  2400, "DS_range1", return_to_initial_values=False) 
scan.scan1D(laser_delay, 110.21, 110.5, 0.01,  2400, "DS_range2", return_to_initial_values=False) 
scan.scan1D(laser_delay, 111, 130, 0.5,  2400, "DS_range3", return_to_initial_values=False) 

lakeshore.set_target_value(207).wait()
time.sleep(30*60) 

laser_delay.set_target_value(112).wait()

mu.set_target_value(21.3).wait()
nu.set_target_value(-125.8).wait()

#Theta scan
scan.scan1D(mu, 20,24,0.2, 1200, "Th_cm", return_to_initial_values=True) 

#DS @ CM
scan.scan1D(laser_delay, 107, 110.2, 0.2,  2400, "DS_range1", return_to_initial_values=False) 
scan.scan1D(laser_delay, 110.21, 110.5, 0.01,  2400, "DS_range2", return_to_initial_values=False) 
scan.scan1D(laser_delay, 111, 130, 0.5,  2400, "DS_range3", return_to_initial_values=False) 

#DS @ ICM
mu.set_target_value(22.5).wait()
nu.set_target_value(-125.4).wait()
scan.scan1D(laser_delay, 107, 110.2, 0.2,  2400, "DS_range1", return_to_initial_values=False) 
scan.scan1D(laser_delay, 110.21, 110.5, 0.01,  2400, "DS_range2", return_to_initial_values=False) 
scan.scan1D(laser_delay, 111, 130, 0.5,  2400, "DS_range3", return_to_initial_values=False) 


lakeshore.set_target_value(212).wait()
time.sleep(30*60) 

laser_delay.set_target_value(112).wait()

mu.set_target_value(21.3).wait()
nu.set_target_value(-125.8).wait()

#Theta scan
scan.scan1D(mu, 20,24,0.2, 1200, "Th_cm", return_to_initial_values=True) 

#DS @ CM
scan.scan1D(laser_delay, 107, 110.2, 0.2,  2400, "DS_range1", return_to_initial_values=False) 
scan.scan1D(laser_delay, 110.21, 110.5, 0.01,  2400, "DS_range2", return_to_initial_values=False) 
scan.scan1D(laser_delay, 111, 130, 0.5,  2400, "DS_range3", return_to_initial_values=False) 

#DS @ ICM
mu.set_target_value(22.5).wait()
nu.set_target_value(-125.4).wait()
scan.scan1D(laser_delay, 107, 110.2, 0.2,  2400, "DS_range1", return_to_initial_values=False) 
scan.scan1D(laser_delay, 110.21, 110.5, 0.01,  2400, "DS_range2", return_to_initial_values=False) 
scan.scan1D(laser_delay, 111, 130, 0.5,  2400, "DS_range3", return_to_initial_values=False) 

