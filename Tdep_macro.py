#%run -i ./script.py 

import time
from epics import caput


#caput("SATES30-LS336:HTR1_RNG", "High")

caput("SATES30-LS336:LOOP1_SP", 10)
caput("SATES30-BRKHF:FLMTR-SET-SETPOINT",45)
time.sleep(10)
scan.scan1D(Thz_delay, 172.8, 174.0, 0.025,  10000, "CDW_midIR_Tdep", return_to_initial_values=True) 

caput("SATES30-LS336:LOOP1_SP", 23)
caput("SATES30-BRKHF:FLMTR-SET-SETPOINT",37)
time.sleep(600)
scan.scan1D(Thz_delay, 172.8, 174.0, 0.025,  10000, "CDW_midIR_Tdep", return_to_initial_values=True) 


caput("SATES30-LS336:LOOP1_SP", 33)
caput("SATES30-BRKHF:FLMTR-SET-SETPOINT",30)
time.sleep(600)
scan.scan1D(Thz_delay, 172.8, 174.0, 0.025,  10000, "CDW_midIR_Tdep", return_to_initial_values=True) 


caput("SATES30-LS336:LOOP1_SP", 43)
caput("SATES30-BRKHF:FLMTR-SET-SETPOINT",25)
time.sleep(600)
scan.scan1D(Thz_delay, 172.8, 174.0, 0.025,  10000, "CDW_midIR_Tdep", return_to_initial_values=True) 


caput("SATES30-LS336:LOOP1_SP", 53)
caput("SATES30-BRKHF:FLMTR-SET-SETPOINT",20)
time.sleep(600)
scan.scan1D(Thz_delay, 172.8, 174.0, 0.025,  10000, "CDW_midIR_Tdep", return_to_initial_values=True) 


caput("SATES30-LS336:LOOP1_SP", 63)
time.sleep(600)
scan.scan1D(Thz_delay, 172.8, 174.0, 0.025,  10000, "CDW_midIR_Tdep", return_to_initial_values=True) 

caput("SATES30-LS336:LOOP1_SP", 73)
time.sleep(600)
scan.scan1D(Thz_delay, 172.8, 174.0, 0.025,  10000, "CDW_midIR_Tdep", return_to_initial_values=True) 

caput("SATES30-LS336:LOOP1_SP", 83)
time.sleep(600)
scan.scan1D(Thz_delay, 172.8, 174.0, 0.025,  10000, "CDW_midIR_Tdep", return_to_initial_values=True) 
