from time import sleep
import numpy as np
from epics import PV

from logzero import logger as log

from slic.core.adjustable import Adjustable
from slic.core.adjustable import PVAdjustable
from slic.core.scanner.scanbackend import wait_for_all #, stop_all
from slic.devices.general.motor import Motor


class LakeShore(Adjustable):
    
    def __init__(self, ID="Temp", units="T", accuracy=0,  name="" ):
         super().__init__(ID, name=name, units=units)
         self.SP_RBV_T=PVAdjustable("SATES30-LS336:LOOP1_SP", "SATES30-LS336:A_RBV", name = "Temp")
         self.accuracy=accuracy

    def set_target_value(self, value):
         t_T= self.SP_RBV_T.set_target_value(value)
         t_T.wait()

    def get_current_value(self):
        return self.SP_RBV_T.get_current_value()
   
    def is_moving(self):
         delta = abs(self.SP_RBV_T.pvs.setvalue.get() - self.SP_RBV_T.pvs.readback.get() )
         return (delta > self.accuracy)

  

         




class Coupled_tth(Adjustable):
     
    
    def __init__(self, ID="tth", units="deg", delta=0, name="" ):
         super().__init__(ID, name=name, units=units)
         self.SRY = Motor("SATES30-ARES:MOT_SRY.VAL")
         self.DRY = Motor("SATES30-ARES:MOT_DRY.VAL")
         self.delta = delta

    def set_target_value(self, value):
         s_SRY = 90 - value  + self.delta
         s_DRY = -2*value
         t_SRY = self.SRY.set_target_value(s_SRY)
         t_DRY = self.DRY.set_target_value(s_DRY)
         t_SRY.wait()
         t_DRY.wait()
    def get_current_value(self):
         return self.DRY.get_current_value()*(-.5)
    def is_moving(self):
         return any([self.SRY.is_moving(),self.DRY.is_moving()])

class Coupled_tth_RIXSside(Adjustable):
     
    
    def __init__(self, ID="tth", units="deg", delta=0, name="" ):
         super().__init__(ID, name=name, units=units)
         self.SRY = Motor("SATES30-ARES:MOT_SRY.VAL")
         self.DRY = Motor("SATES30-ARES:MOT_DRY.VAL")
         self.delta = delta

    def set_target_value(self, value):
         s_SRY = value -90 + self.delta
         s_DRY = 2*value-199.9
         t_SRY = self.SRY.set_target_value(s_SRY)
         t_DRY = self.DRY.set_target_value(s_DRY)
         t_SRY.wait()
         t_DRY.wait()
    def get_current_value(self):
         return (self.DRY.get_current_value()*(-1)-199.9)*(-.5)
    def is_moving(self):
         return any([self.SRY.is_moving(),self.DRY.is_moving()])


class Coupled_tth_outer_RixsSide(Adjustable):
     
    
    def __init__(self, ID="tth", units="deg", delta=0, name="" ):
         super().__init__(ID, name=name, units=units)
         self.SRY = Motor("SATES30-ARES:MOT_SRY.VAL")
         self.twoTRY = Motor("SATES30-ARES:MOT_2TRY.VAL")
         self.delta = delta

    def set_target_value(self, value):
         s_SRY =-(90-value)+self.delta  
         s_2TRY = 124.4 -2*value

         t_SRY = self.SRY.set_target_value(s_SRY)
         t_2TRY = self.twoTRY.set_target_value(s_2TRY)
         t_SRY.wait()
         t_2TRY.wait()
    def get_current_value(self):
         return (124.4-self.twoTRY.get_current_value())*0.5
    def is_moving(self):
         return any([self.SRY.is_moving(),self.twoTRY.is_moving()])





class Coupled_THzGate(Adjustable):
     
    
    def __init__(self, ID="THzGate", units="mm",  name="" ):
         super().__init__(ID, name=name, units=units)
         self.THz = Motor("SLAAT31-LMOT-M806:MOT")
         self.Gate = Motor("SLAAT31-LMOT-M808:MOT")
         #self.delta = delta
         

    def set_target_value(self, value):
         s_THz  = value  
         s_Gate = value - ( self.THz.get_current_value() - self.Gate.get_current_value())
         
         t_THz = self.THz.set_target_value(s_THz)
         t_Gate = self.Gate.set_target_value(s_Gate)
         t_THz.wait()
         t_Gate.wait()
    def get_current_value(self):
         return self.THz.get_current_value()
    def is_moving(self):
         return any([self.THz.is_moving(),self.Gate.is_moving()])


class Coupled_THzGate_fixedDelta(Adjustable):
 

    def __init__(self, ID="THzGate_fixed", units="mm", delta=0, name="" ):
          super().__init__(ID, name=name, units=units)
          self.THz = Motor("SLAAT31-LMOT-M806:MOT")
          self.Gate = Motor("SLAAT31-LMOT-M808:MOT")
          self.delta = delta
 
 
    def set_target_value(self, value):
          s_THz  = value
          s_Gate = value - self.delta 
 
          t_THz = self.THz.set_target_value(s_THz)
          t_Gate = self.Gate.set_target_value(s_Gate)
          t_THz.wait()
          t_Gate.wait()
    def get_current_value(self):
          return self.THz.get_current_value()
    def is_moving(self):
          return any([self.THz.is_moving(),self.Gate.is_moving()])


