#%run -i ./script.py 

import time
from epics import caput


caput("SATOP11-OSGM087:EXITSLIT", 50)
caput("SLAAT31-LMOT-M806:MOT.VAL", 173.02)
scan.scan1D(mu, -70, -50, 0.5,  4000, "rock_curve_t0", return_to_initial_values=True) 

caput("SLAAT31-LMOT-M806:MOT.VAL", 173.6)
scan.scan1D(mu, -70, -50, 0.5,  4000, "rock_curve_aft_t0", return_to_initial_values=True) 

caput("SLAAT31-LMOT-M806:MOT.VAL", 172.5)
scan.scan1D(mu, -70, -50, 0.5,  4000, "rock_curve_bef_t0", return_to_initial_values=True) 



caput("SATOP11-OSGM087:EXITSLIT", 100)


mu.set_target_value(-60.8)
time.sleep(20)
mu.park()


scan.scan1D(Thz_delay, 172.5, 174.35, 0.02,  3000, "CDW_midIR_20K", return_to_initial_values=True) 
scan.scan1D(Thz_delay, 172.5, 174.35, 0.02,  3000, "CDW_midIR_20K", return_to_initial_values=True) 
scan.scan1D(Thz_delay, 172.5, 174.35, 0.02,  3000, "CDW_midIR_20K", return_to_initial_values=True) 
scan.scan1D(Thz_delay, 172.5, 174.35, 0.02,  3000, "CDW_midIR_20K", return_to_initial_values=True) 
scan.scan1D(Thz_delay, 172.5, 174.35, 0.02,  3000, "CDW_midIR_20K", return_to_initial_values=True) 
scan.scan1D(Thz_delay, 172.5, 174.35, 0.02,  3000, "CDW_midIR_20K", return_to_initial_values=True) 

