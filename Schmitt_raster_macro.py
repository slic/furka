#%run -i ./script.py 

import time
from epics import caput

### rastering sample and scannign a few delays

deltaY = 0.04

d1 = 62   #before t0
d2 = 62.9 #1.3 ps 
d3 = 63.5 #5.3 ps


ymax = 2.89

y = 2.45

TY.set_target_value(y).wait()
time.sleep(5)
while(y<ymax):
    for d in ([d1,d2,d3]):
        print('>>>>>>>>>>>>>>>STY = '+str(y)+' mm<<<<<<<<<<<<<<<<<<<<')    
        print('########################## Acquiring Run ##########################')
        daq.acquire("Raster_delay"+str(d1), n_pulses=12000)    
        twomicron_delay.set_target_value(d).wait()
        print('>>>>>>>>>>>>>Delay set to' +str(d)+' mm<<<<<<<<<<<<<<<<<<<<<<')
        TY.set_target_value(y)
        time.sleep(5)
    y = y+deltaY
        


