#%run -i ./script.py 

import time

## 800 shutter: SLAAT31-LDIO-LAS6411:SET_BO02
# RIXS scans opening/closing the laser shutter 

#set time delay in mm
#gate_delay.set_target_value(100).wait()

caput("SLAAT31-LDIO-LAS6411:SET_BO02",1) ## laser shutter open
daq.acquire("RIXS_on", n_pulses=30000, n_repeat=3, wait=True)

caput("SLAAT31-LDIO-LAS6411:SET_BO02",1) ## laser shutter open
daq.acquire("RIXS_on", n_pulses=30000, n_repeat=3, wait=True)

caput("SLAAT31-LDIO-LAS6411:SET_BO02",0) ## laser shutter closed
daq.acquire("RIXS_off", n_pulses=30000, n_repeat=3, wait=True)

caput("SLAAT31-LDIO-LAS6411:SET_BO02",1) ## laser shutter open
daq.acquire("RIXS_on", n_pulses=30000, n_repeat=3, wait=True)

caput("SLAAT31-LDIO-LAS6411:SET_BO02",1) ## laser shutter open
daq.acquire("RIXS_on", n_pulses=30000, n_repeat=3, wait=True)

caput("SLAAT31-LDIO-LAS6411:SET_BO02",1) ## laser shutter open
daq.acquire("RIXS_on", n_pulses=30000, n_repeat=3, wait=True)

caput("SLAAT31-LDIO-LAS6411:SET_BO02",0) ## laser shutter closed
daq.acquire("RIXS_off", n_pulses=30000, n_repeat=3, wait=True)

caput("SLAAT31-LDIO-LAS6411:SET_BO02",1) ## laser shutter open
daq.acquire("RIXS_on", n_pulses=30000, n_repeat=3, wait=True)


#caput("SLAAT31-LDIO-LAS6411:SET_BO02",1) ## laser shutter open
#daq.acquire("RIXS_on", n_pulses=30000, n_repeat=3, wait=True)

#caput("SLAAT31-LDIO-LAS6411:SET_BO02",0) ## laser shutter closed
#daq.acquire("RIXS_off", n_pulses=30000, n_repeat=3, wait=True)

#caput("SLAAT31-LDIO-LAS6411:SET_BO02",1) ## laser shutter open
#daq.acquire("RIXS_on", n_pulses=30000, n_repeat=3, wait=True)

#caput("SLAAT31-LDIO-LAS6411:SET_BO02",0) ## laser shutter closed
#daq.acquire("RIXS_off", n_pulses=30000, n_repeat=3, wait=True)

#caput("SLAAT31-LDIO-LAS6411:SET_BO02",1) ## laser shutter open
#daq.acquire("RIXS_on", n_pulses=30000, n_repeat=3, wait=True)

#caput("SLAAT31-LDIO-LAS6411:SET_BO02",0) ## laser shutter closed
#daq.acquire("RIXS_off", n_pulses=30000, n_repeat=3, wait=True)

#caput("SLAAT31-LDIO-LAS6411:SET_BO02",1) ## laser shutter open
#daq.acquire("RIXS_on", n_pulses=30000, n_repeat=3, wait=True)

#caput("SLAAT31-LDIO-LAS6411:SET_BO02",0) ## laser shutter closed
#daq.acquire("RIXS_off", n_pulses=30000, n_repeat=3, wait=True)


#caput("SLAAT31-LDIO-LAS6411:SET_BO02",0)
#daq.acquire("RIXS_off", n_pulses=30000, wait=True)
#daq.acquire("RIXS_off", n_pulses=30000, wait=True)

#caput("SLAAT31-LDIO-LAS6411:SET_BO02",1)
#daq.acquire("RIXS_on", n_pulses=30000, wait=True)
#daq.acquire("RIXS_on", n_pulses=30000, wait=True)
#caput("SLAAT31-LDIO-LAS6411:SET_BO02",0)
#daq.acquire("RIXS_off", n_pulses=30000, wait=True)
#daq.acquire("RIXS_off", n_pulses=30000, wait=True)

#gate_delay.set_target_value(112).wait()


