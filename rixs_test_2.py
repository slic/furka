#%run -i ./script.py 

import time

### start from 100 K
### RIXS at 4 energies
mono_new.set_target_value(528.0).wait()
time.sleep(10)

mono_new.set_target_value(529.5).wait()
daq.acquire("YBCFO_100K_529p5V", n_pulses=30000)

mono_new.set_target_value(530.2).wait()


daq.acquire("YBCFO_100K_530p2eV", n_pulses=30000, n_repeat = 2)
scan.acquire(3, 10000, 'Name')





mono_new.set_target_value(530.9).wait()
daq.acquire("YBCFO_100K_530p9eV", n_pulses=30000)

mono_new.set_target_value(531.7).wait()
daq.acquire("YBCFO_100K_531p7eV", n_pulses=30000)

mono_new.set_target_value(533.3).wait()
daq.acquire("YBCFO_100K_533p3eV", n_pulses=30000)

mono_new.set_target_value(528.0).wait()

#############################
## change to 150 K
caput("SATES30-BRKHF:FLMTR-SET-SETPOINT",10)
caput("SATES30-LS336:LOOP1_SP",135)
time.sleep(600)
print('10min')
time.sleep(600)
print('20min')
time.sleep(600)
print('30min')
time.sleep(600)
print('40min')
time.sleep(600)
print('50min')
time.sleep(600)
print('60min')


### RIXS at 4 energies
mono_new.set_target_value(529.5).wait()
daq.acquire("YBCFO_150K_529p5V", n_pulses=30000)

mono_new.set_target_value(530.2).wait()
daq.acquire("YBCFO_150K_530p2eV", n_pulses=30000)

mono_new.set_target_value(530.9).wait()
daq.acquire("YBCFO_150K_530p9eV", n_pulses=30000)

mono_new.set_target_value(531.7).wait()
daq.acquire("YBCFO_150K_531p7eV", n_pulses=30000)

mono_new.set_target_value(533.3).wait()
daq.acquire("YBCFO_150K_533p3eV", n_pulses=30000)

mono_new.set_target_value(528.0).wait()

#############################
## change to 200 K
caput("SATES30-BRKHF:FLMTR-SET-SETPOINT",5)
caput("SATES30-LS336:LOOP1_SP",185)
time.sleep(600)
print('10min')
time.sleep(600)
print('20min')
time.sleep(600)
print('30min')
time.sleep(600)
print('40min')
time.sleep(600)
print('50min')
time.sleep(600)
print('60min')

### RIXS at 4 energies
mono_new.set_target_value(529.5).wait()
daq.acquire("YBCFO_200K_529p5V", n_pulses=30000)

mono_new.set_target_value(530.2).wait()
daq.acquire("YBCFO_200K_530p2eV", n_pulses=30000)

mono_new.set_target_value(530.9).wait()
daq.acquire("YBCFO_200K_530p9eV", n_pulses=30000)

mono_new.set_target_value(531.7).wait()
daq.acquire("YBCFO_200K_531p7eV", n_pulses=30000)

mono_new.set_target_value(533.3).wait()
daq.acquire("YBCFO_200K_533p3eV", n_pulses=30000)

mono_new.set_target_value(528.0).wait()
#############################
## change to 250 K
caput("SATES30-LS336:LOOP1_SP",235)
time.sleep(600)
print('10min')
time.sleep(600)
print('20min')
time.sleep(600)
print('30min')
time.sleep(600)
print('40min')
time.sleep(600)
print('50min')
time.sleep(600)
print('60min')

### RIXS at 4 energies

mono_new.set_target_value(529.5).wait()
daq.acquire("YBCFO_250K_529p5V", n_pulses=30000)

mono_new.set_target_value(530.2).wait()
daq.acquire("YBCFO_250K_530p2eV", n_pulses=30000)

mono_new.set_target_value(530.9).wait()
daq.acquire("YBCFO_250K_530p9eV", n_pulses=30000)

mono_new.set_target_value(531.7).wait()
daq.acquire("YBCFO_250K_531p7eV", n_pulses=30000)

mono_new.set_target_value(533.3).wait()
daq.acquire("YBCFO_250K_533p3eV", n_pulses=30000)

