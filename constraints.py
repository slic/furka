#!/usr/bin/env python3

from math import inf


class ExtraConstraint:

    def __init__(self, **kwargs):
        self.kwargs = _unpack(kwargs)

    def __call__(self, x):
        kwargs = self.kwargs
        for name, value in x.items():
            vmin = kwargs.get(f"{name}_min", -inf)
            vmax = kwargs.get(f"{name}_max", +inf)
            check = (vmin <= value < vmax)
#            print(name, f"\t{vmin} <= {value} < {vmax}\t->", check)
            if not check:
                return False
        return True



def _unpack(d):
    res = {}
    for k, v in d.items():
        if k.endswith("_min") or k.endswith("_max"):
            res[k] = v
        else:
            try:
                vmin, vmax = v
            except (ValueError, TypeError) as e:
                raise ValueError(f"cannot unpack {k} = {v} into ({k}_min, {k}_max), need exactly 2 values") from e
            res[k + "_min"] = vmin
            res[k + "_max"] = vmax
    if res != d:
        print(f"unpacked: {d} -> {res}")
    return res



if __name__ == "__main__":

    cons = ExtraConstraint(beta_min=0, beta_max=10, gamma_max=33)


    example = {
        "alpha": 1,
        "beta": 1
    }

    assert cons(example)


    example = {
        "alpha": 1,
        "beta": 11
    }

    assert not cons(example)


    example = {
        "gamma": 3
    }

    assert cons(example)


    example = {
        "gamma": 333
    }

    assert not cons(example)


    cons = ExtraConstraint(alpha=(1, 100))

    example = {
        "alpha": 10
    }

    assert cons(example)

    example = {
        "alpha": 1000
    }

    assert not cons(example)


    badcons = ExtraConstraint(test=33)



