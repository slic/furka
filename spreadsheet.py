from slic.core.adjustable import PVAdjustable, PVEnumAdjustable
from slic.core.device import SimpleDevice
from slic.utils import as_shortcut


class PVStringAdjustable(PVAdjustable):
    def get_current_value(self):
        return str(self.pvs.readback.get(as_string=True)).strip()



n_unds = [
    6, 7, 8, 9, 10, 11, 12, 13, # 14 is the CHIC
    15, 16, 17, 18, 19, 20, 21, 22
]

undulator_info = {}
for i in n_unds:
    undulator_info[f"energy{i}"]       = PVAdjustable(f"SATUN{i:02}-UIND030:FELPHOTENE",  internal=True)
    undulator_info[f"polarisation{i}"] = PVEnumAdjustable(f"SATUN{i:02}-UIND030:POL-SET", internal=True)



spreadsheet_info = dict(


    FELrepRate     	  = PVAdjustable("SWISSFEL-STATUS:Bunch-2-Appl-Freq-RB", internal=True),
    Diffr2TRY  	          = PVAdjustable("SATES30-ARES:MOT_2TRY.VAL", internal=True),
    DiffrDRY              = PVAdjustable("SATES30-ARES:MOT_DRY.VAL", internal=True),
    DiffrTX               = PVAdjustable("SATES30-ARES:MOT_STX.VAL", internal=True),
    DiffrTY               = PVAdjustable("SATES30-ARES:MOT_STY.VAL", internal=True),
    DiffrTZ               = PVAdjustable("SATES30-ARES:MOT_STZ.VAL", internal=True),
    DiffrRX               = PVAdjustable("SATES30-ARES:MOT_SRX.VAL", internal=True),
    DiffrRY               = PVAdjustable("SATES30-ARES:MOT_SRY.VAL", internal=True),
    DiffrRZ               = PVAdjustable("SATES30-ARES:MOT_SRZ.VAL", internal=True),
    KBV_BU                = PVAdjustable("SATOP31-OKBV178:BU.RBV", internal=True),
    KBV_BD                = PVAdjustable("SATOP31-OKBV178:BD.RBV", internal=True),


  

    att64              = PVStringAdjustable("SATFE10-OATT064:MOT2TRANS.VALD", internal=True),
    att65              = PVStringAdjustable("SATFE10-OATT065:MOT2TRANS.VALD", internal=True),

    photonEnergy          = PVAdjustable("SATUN:FELPHOTENE", internal=True),

    **undulator_info
)



overview = SimpleDevice("Furka Overview", **spreadsheet_info)


spreadsheet_line = [
    "timeStamp", "run name", "File name", "comments", "static/scan", "start", "stop", "step size", "shots",  "Sample",
    "ppressChamb3",
    "photonEnergy",
    "pulse_energy",
    "FELrepRate",
    "att64",
    "att65",
    "energy6",
    "energy7",
    "energy8",
    "energy9",
    "energy10",
    "energy11",
    "energy12",
    "energy13",
    "energy14",
    "energy15",
    "energy16",
    "energy17",
    "energy18",
    "energy19",
    "energy20",
    "energy21",
    "energy22",
]



@as_shortcut
def print_overview():
    print(overview)

@as_shortcut
def print_line_for_spreadsheet():
    ov = overview.__dict__
    def get(i):
        if i in ov:
            return str(ov[i].get())
        return ""
    res = [get(i) for i in spreadsheet_line]
    res = ",".join(res)
    print(res)


