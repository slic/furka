from slic.devices.general.motor import Motor
from slic.core.device import SimpleDevice

##gui(extras={"Tweak SASE Spec": "Tweak"})



us_gr = SimpleDevice(
    "SASE Spec US Grating",
    gr_x = Motor("SATOP11-PSAS079:MOT_UG_X", name="SASE US Grating X"),
    gr_y = Motor("SATOP11-PSAS079:MOT_UG_Y", name="SASE US Grating Y"),
    gr_z = Motor("SATOP11-PSAS079:MOT_UG_Z", name="SASE US Grating Z"),
    #...
)


ds_det = SimpleDevice(
    "SASE Spec DS Detector",
    filt_x = Motor("SATOP11-PSAS079:MOT_DDF_X", name="SASE DS Det Filt X"),
    slit1_y = Motor("SATOP11-PSAS079:MOT_DDS_Y1", name="SASE DS Det Slit1 Y"),
    slit2_y = Motor("SATOP11-PSAS079:MOT_DDS_Y2", name="SASE DS Det Slit2 Y"),
    cryst_rx = Motor("SATOP11-PSAS079:MOT_DDC_RX", name="SASE DS YAG RX"),
    cryst_x = Motor("SATOP11-PSAS079:MOT_DDC_X", name="SASE DS YAG X"), 
    cryst_y = Motor("SATOP11-PSAS079:MOT_DDC_Y", name="SASE DS YAG Y"), 
    cam_y = Motor("SATOP11-PSAS079:MOT_DDCAM_Y", name="SASE DS Camera Y"), 
)

ds_gr = SimpleDevice(
    "SASE Spec DS Grating",
    grd_x = Motor("SATOP11-PSAS079:MOT_DG_X", name="SASE DS Grating X"),
    grd_y = Motor("SATOP11-PSAS079:MOT_DG_Y", name="SASE DS Grating Y"),
    grd_z = Motor("SATOP11-PSAS079:MOT_DG_Z", name="SASE DS Grating Z"),
    grd_rz = Motor("SATOP11-PSAS079:MOT_DG_RZ", name="SASE DS Grating RZ"),

)


sasespecs = SimpleDevice(
    "SASE Spectrometer",
    us_gr = us_gr, 
    ds_det = ds_det, 
    ds_gr = ds_gr, 
)


