def cmos_live(sleep=0.1):
	import numpy as np
	from epics import caget, caput, cainfo 
	from matplotlib import pyplot as plt
	import time

	data = caget('SATES31-CAMS187-RIXS1:FPICTURE')
	x_size = int(caget('SATES31-CAMS187-RIXS1:CAMROI_X_END')-caget('SATES31-CAMS187-RIXS1:CAMROI_X_START')+1)
	y_size = int(len(data)/x_size) 

	xx = np.arange(1,x_size+1,1)
	ROI = [data[i:i+x_size] for i in range(0,int(len(data)),x_size)] 

	plt.ion()
	fig = plt.figure()
	image = plt.imshow(ROI)
	plt.colorbar()
	plt.clim(0,np.max(data))

	plt.title('live PCO image',fontsize=20)
	plt.xlabel('x [px]',fontsize=16)
	plt.ylabel('y [px]',fontsize=16)

	for _ in iter(int,1):
		data = caget('SATES31-CAMS187-RIXS1:FPICTURE') 
		ROI = [data[i:i+x_size] for i in range(0,int(len(data)),x_size)] 
		image.set_data(ROI)
		fig.canvas.draw()
		fig.canvas.flush_events()
		plt.clim(0,np.max(data))
		time.sleep(sleep)

