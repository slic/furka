#%run -i ./script.py 

import time

#############################
## change to 150 K
#caput("SATES30-BRKHF:FLMTR-SET-SETPOINT",10)
#caput("SATES30-LS336:LOOP1_SP",135)
#time.sleep(600)
#print('10min')
#time.sleep(600)
#print('20min')

### RIXS at 4 energies
mono_new.set_target_value(930.6).wait()
daq.acquire("YBCFO_150K_930p6eV", n_pulses=30000)

mono_new.set_target_value(932.2).wait()
daq.acquire("YBCFO_150K_932p2eV", n_pulses=30000)

mono_new.set_target_value(933.0).wait()
daq.acquire("YBCFO_150K_933p0eV", n_pulses=30000)

mono_new.set_target_value(934.2).wait()
daq.acquire("YBCFO_150K_934p2eV", n_pulses=30000)


#############################
## change to 200 K
caput("SATES30-BRKHF:FLMTR-SET-SETPOINT",5)
caput("SATES30-LS336:LOOP1_SP",185)
time.sleep(600)
print('10min')
time.sleep(600)
print('20min')
time.sleep(600)
print('30min')
time.sleep(600)
print('40min')
time.sleep(600)
print('50min')
time.sleep(600)
print('60min')

### RIXS at 4 energies
mono_new.set_target_value(930.6).wait()
daq.acquire("YBCFO_200K_930p6eV", n_pulses=30000)

mono_new.set_target_value(932.2).wait()
daq.acquire("YBCFO_200K_932p2eV", n_pulses=30000)

mono_new.set_target_value(933.0).wait()
daq.acquire("YBCFO_200K_933p0eV", n_pulses=30000)

mono_new.set_target_value(934.2).wait()
daq.acquire("YBCFO_200K_934p2eV", n_pulses=30000)

#############################
## change to 250 K
caput("SATES30-LS336:LOOP1_SP",235)
time.sleep(600)
print('10min')
time.sleep(600)
print('20min')
time.sleep(600)
print('30min')
time.sleep(600)
print('40min')
time.sleep(600)
print('50min')
time.sleep(600)
print('60min')

### RIXS at 4 energies
mono_new.set_target_value(930.6).wait()
daq.acquire("YBCFO_250K_930p6eV", n_pulses=30000)

mono_new.set_target_value(932.2).wait()
daq.acquire("YBCFO_250K_932p2eV", n_pulses=30000)

mono_new.set_target_value(933.0).wait()
daq.acquire("YBCFO_250K_933p0eV", n_pulses=30000)

mono_new.set_target_value(934.2).wait()
daq.acquire("YBCFO_250K_934p2eV", n_pulses=30000)

